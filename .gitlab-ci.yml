image: docker:19.03.7
services:
  - docker:19.03.7-dind
stages:
  - Build
  - Push
  - Update Chart

##############################################################################
##                              Variables                                   ##
##############################################################################
variables:
  AWS_WEB_IDENTITY_TOKEN_FILE: /tmp/web-identity-token
  CD_CHART_REPO: nginx-webserver-chart
  CD_GIT_REPOSITORY: git@gitlab.com:calvine-devops/gitops-argocd-demo/$CD_CHART_REPO.git
  CD_MANIFEST_FILE: values.yaml
  APP_NAME: gitops-argocd-demo
  CHART_FOLDER: helm

##############################################################################
##                Setup OIDC Assume Role Template                          ##
##############################################################################
.assume_role: &assume_role
  before_script:
    - apk add python3
    - pip3 install awscli
    - >
      export $(printf "AWS_ACCESS_KEY_ID=%s AWS_SECRET_ACCESS_KEY=%s AWS_SESSION_TOKEN=%s"
      $(aws sts assume-role-with-web-identity
      --role-arn $AWS_ROLE_ARN
      --role-session-name "GitLabRunner-${CI_PROJECT_ID}-${CI_PIPELINE_ID}"
      --web-identity-token $CI_JOB_JWT_V2
      --duration-seconds 3600
      --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]'
      --output text))
    - aws sts get-caller-identity
    - $(aws ecr get-login --no-include-email --region eu-west-1)

##############################################################################
##                Setup and Enable SSH                          ##
##############################################################################
.enable_ssh: &enable_ssh |-
    apk add --no-cache git
    mkdir -p /root/.ssh
    echo "$SSH_PRIVATE_KEY" > /root/.ssh/id_rsa
    ssh-keyscan -H gitlab.com > /root/.ssh/known_hosts
    chmod 600 /root/.ssh/id_rsa

##############################################################################
##                             Build Image                                 ##
##############################################################################
Build:
  <<: *assume_role
  stage: Build
  script:
    - docker build --compress -t $ECR_REPO:$CI_COMMIT_SHORT_SHA .
    - docker push $ECR_REPO:$CI_COMMIT_SHORT_SHA

##############################################################################
##                       Push to ECR                                       ##
##############################################################################
Push:
  stage: Push
  <<: *assume_role
  script:
    - docker pull $ECR_REPO:$CI_COMMIT_SHORT_SHA
    - docker tag $ECR_REPO:$CI_COMMIT_SHORT_SHA $ECR_REPO:latest
    - docker push $ECR_REPO:latest

update_chart_manifest:
  stage: Update Chart
  only:
    - main
  needs:
    - Push
  variables:
    GIT_STRATEGY: none
  retry: 2
  before_script:
    - *enable_ssh
  script:
    # Configure Git
    - git config --global user.name $APP_NAME
    - git config --global user.email $APP_NAME"@gitlab.com"
    - git clone --single-branch --branch main $CD_GIT_REPOSITORY
    - cd $CD_CHART_REPO
    - cd $CHART_FOLDER
    # HELM Update
    - >
      tag=$(cat values.yaml | grep tag: | awk '{print $2}')
    - sed -i "s/$tag/$CI_COMMIT_SHORT_SHA/" values.yaml
    - cat $CD_MANIFEST_FILE
    - cd ..
    - git commit -am "🔥 update image tag" && git push origin main

# Nginx Webserver ArgoCD Demo



## Usage

Check this
[GitOps with ArgoCD, EKS and GitLab CI using Terraform)](https://medium.com/@calvineotieno010/gitops-with-argocd-eks-and-gitlab-ci-using-terraform-2a3c094b4ea3)
for detailed explanation.

### Environment Variables

These Environment Variables are needed for the pipeline when
runnig Terraform commands.

* `AWS_ROLE ARN` - AWS Role Arn to used by the pipeline to get temporary credentials
* `ECR_REPO ` -  ECR URI where we are pushing the image
* `SSH_PRIVATE_KEY`  -  SSH Key for updating the chart manifest repository
